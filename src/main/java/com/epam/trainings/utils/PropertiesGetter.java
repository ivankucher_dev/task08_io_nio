package com.epam.trainings.utils;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;


public class PropertiesGetter {
  private static Logger log = LogManager.getLogger(PropertiesGetter.class.getName());

  public static Properties getProperties() {
    Properties prop = null;
    try (InputStream input =
        PropertiesGetter.class.getClassLoader().getResourceAsStream("config.properties")) {

      prop = new Properties();

      if (input == null) {
        log.error("Sorry, unable to find game.properties");
        return null;
      }
      prop.load(input);
    } catch (IOException ex) {
      ex.printStackTrace();
    }
    return prop;
  }

public static String getProperty(String prop){
    return getProperties().getProperty(prop);
}
  public static int getIntProp(String prop) {
    return Integer.valueOf(getProperties().getProperty(prop));
  }
}
