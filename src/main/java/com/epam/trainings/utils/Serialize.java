package com.epam.trainings.utils;

import java.io.*;

public class Serialize<T extends Serializable> {

  public void write(String path, T object) {
    try (ObjectOutputStream oos =
        new ObjectOutputStream(new BufferedOutputStream(new FileOutputStream(path)))) {
      oos.writeObject(object);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public T read(String path) {
    T object = null;
    try (ObjectInputStream objectInputStream =
        new ObjectInputStream(new BufferedInputStream(new FileInputStream(path)))) {
      object = (T) objectInputStream.readObject();
    } catch (IOException | ClassNotFoundException e) {
      e.printStackTrace();
    }
    return object;
  }
}
