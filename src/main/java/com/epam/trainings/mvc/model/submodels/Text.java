package com.epam.trainings.mvc.model.submodels;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import static com.epam.trainings.utils.PropertiesGetter.*;

public class Text {

  private static final String READ_FILE_PATH = getProperty("read_text_path");
  private static final String WRITE_FILE_PATH = getProperty("write_text_path");
  private static final int BUFFER_SIZE = getIntProp("buffer_size");

  public Map startTest() {
    String text = readWithBuffer();
    double readDefaultBuffer = readWithBuffer(READ_FILE_PATH);
    double readLowBuffer = readWithBuffer(READ_FILE_PATH, 1024);
    double readNoBuffer = readWithoutBuffer(READ_FILE_PATH);
    double writeWithoutBuffer = writeWithoutBuffer(WRITE_FILE_PATH, text);
    double writeWithBuffer = writeWithBuffer(WRITE_FILE_PATH, text);
    Map<Double, String> result = new HashMap<>();
    result.put(readDefaultBuffer, "Read by default buffer " + BUFFER_SIZE + " took : ");
    result.put(readLowBuffer, "Read by 1024  buffer took : ");
    result.put(readNoBuffer, "Read without buffer took : ");
    result.put(writeWithBuffer, "Write by buffer took : ");
    result.put(writeWithoutBuffer, "Write without buffer took : ");
    return result;
  }

  public Double readWithBuffer(String path, int buffer) {
    long start = System.currentTimeMillis();
    StringBuilder text = new StringBuilder();
    try (FileInputStream fs = new FileInputStream(new File(READ_FILE_PATH))) {
      BufferedInputStream br = new BufferedInputStream(fs, buffer);
      int ch;
      while ((ch = br.read()) != -1) {
        text.append((char) ch);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    long end = System.currentTimeMillis();

    return (double) (end - start) / 1000;
  }

  public String readWithBuffer() {
    StringBuilder text = new StringBuilder();
    try (FileInputStream fs = new FileInputStream(new File(READ_FILE_PATH))) {
      BufferedInputStream br = new BufferedInputStream(fs);
      int ch;
      while ((ch = br.read()) != -1) {
        text.append((char) ch);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return text.toString();
  }

  public Double readWithoutBuffer(String path) {
    long start = System.currentTimeMillis();
    StringBuilder text = new StringBuilder();
    try (FileInputStream fs = new FileInputStream(new File(READ_FILE_PATH))) {
      int ch;
      while ((ch = fs.read()) != -1) {
        text.append((char) ch);
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    long end = System.currentTimeMillis();
    return (double) (end - start) / 1000;
  }

  public double writeWithoutBuffer(String path, String text) {
    long start = System.currentTimeMillis();
    try (DataOutputStream os =
        new DataOutputStream(new FileOutputStream(new File(WRITE_FILE_PATH)))) {
      os.writeChars(text);
    } catch (IOException e) {
      e.printStackTrace();
    }
    long end = System.currentTimeMillis();
    return (double) (end - start) / 1000;
  }

  public double writeWithBuffer(String path, String text) {
    return writeWithBuffer(path, text, BUFFER_SIZE);
  }

  public Double readWithBuffer(String path) {
    return readWithBuffer(path, BUFFER_SIZE);
  }

  public double writeWithBuffer(String path, String text, int buffer) {
    long start = System.currentTimeMillis();
    try (DataOutputStream os =
        new DataOutputStream(
            new BufferedOutputStream(new FileOutputStream(new File(path)), buffer))) {
      os.writeChars(text);
    } catch (IOException e) {
      e.printStackTrace();
    }
    long end = System.currentTimeMillis();
    return (double) (end - start) / 1000;
  }
}
