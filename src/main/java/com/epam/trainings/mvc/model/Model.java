package com.epam.trainings.mvc.model;

import com.epam.trainings.mvc.model.submodels.Droid;
import com.epam.trainings.mvc.model.submodels.Ship;
import com.epam.trainings.mvc.model.submodels.Text;

import java.util.ArrayList;
import java.util.Map;

public class Model {
  private Ship ship;
  private Text text;

  public Model() {
    text = new Text();
  }

  public Ship getShip() {
    Droid droid1 = new Droid(1, "James", 45, "Mars");
    Droid droid2 = new Droid(2, "Carl", 1234, "Moon");
    Droid droid3 = new Droid(3, "Kenny", 533, "Earth");
    ArrayList<Droid> droids = new ArrayList<Droid>();
    droids.add(droid1);
    droids.add(droid2);
    droids.add(droid3);
    ship = new Ship(2, droids, "Moon", "StarShip");
    return ship;
  }

  public Map runTextBuffer() {
    Map result = text.startTest();
    return result;
  }
}
