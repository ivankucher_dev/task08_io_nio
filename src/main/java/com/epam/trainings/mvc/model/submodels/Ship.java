package com.epam.trainings.mvc.model.submodels;

import java.io.Serializable;
import java.util.ArrayList;

public class Ship implements Serializable {
  private int id;
  private ArrayList<Droid> droids;
  private transient String wayToGo;
  private String name;

  public Ship(int id, ArrayList<Droid> droids, String wayToGo, String name) {
    this.id = id;
    this.droids = droids;
    this.wayToGo = wayToGo;
    this.name = name;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public ArrayList<Droid> getDroids() {
    return droids;
  }

  public void setDroids(ArrayList<Droid> droids) {
    this.droids = droids;
  }

  public String getWayToGo() {
    return wayToGo;
  }

  public void setWayToGo(String wayToGo) {
    this.wayToGo = wayToGo;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    droids.forEach(droid -> sb.append(droid.toString() + ", "));
    return "Ship with id = "
        + id
        + ", name - "
        + name
        + ", droids =[ "
        + sb.toString()
        + " ] , way = "
        + wayToGo;
  }
}
