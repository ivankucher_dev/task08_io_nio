package com.epam.trainings.mvc.model.submodels;

import java.util.ArrayList;
import java.util.List;

public class Message implements ClientServerMessage {

  private String message;
  private List<String> messages;

  public Message(String initMessage) {
    this.message = initMessage;
    this.messages = new ArrayList<>();
  }

  public void setMessage(String message) {
    this.message = message;
  }

  public void createMessages(List<String> messages) {
    this.messages = messages;
  }

  public List<String> getMessages() {
    return messages;
  }

  public void setMessages(List<String> messages) {
    this.messages = messages;
  }

  @Override
  public String createMessage(String message) {
    this.message = message;
    return this.message;
  }

  @Override
  public String getMessage() {
    return this.message;
  }

  @Override
  public byte[] getMessageAsByteArray() {
    byte[] byteArray = this.message.getBytes();
    return byteArray;
  }

  @Override
  public String toString() {
    return this.message;
  }
}
