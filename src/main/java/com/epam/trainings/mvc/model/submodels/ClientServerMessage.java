package com.epam.trainings.mvc.model.submodels;

import java.util.List;

public interface ClientServerMessage {
  String createMessage(String message);

  String getMessage();

  byte[] getMessageAsByteArray();

  void setMessage(String message);

  void createMessages(List<String> messages);

  List<String> getMessages();

  void setMessages(List<String> messages);
}
