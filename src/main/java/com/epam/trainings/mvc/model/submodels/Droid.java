package com.epam.trainings.mvc.model.submodels;

import java.io.Serializable;

public class Droid implements Serializable {
  private int id;
  private String name;
  private double capacity;
  private transient String location;

  public Droid(int id, String name, double capacity, String location) {
    this.id = id;
    this.name = name;
    this.capacity = capacity;
    this.location = location;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public double getCapacity() {
    return capacity;
  }

  public void setCapacity(double capacity) {
    this.capacity = capacity;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  @Override
  public String toString() {
    return "[Droid "
        + id
        + " - "
        + name
        + ", capacity = "
        + capacity
        + ", location = "
        + location
        + "]";
  }
}
