package com.epam.trainings.mvc.controller;

import com.epam.trainings.mvc.model.submodels.ClientServerMessage;
import com.epam.trainings.mvc.model.submodels.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

public class NioServerController {

  private static Logger log = LogManager.getLogger(NioServerController.class.getName());
  private static ClientServerMessage message;
  public static final int PORT = 1111;
  public static final String HOST = "localhost";
  private static ServerSocketChannel crunchifySocket;
  private static InetSocketAddress crunchifyAddr;
  private static Selector selector;
  private static ArrayList<SocketChannel> users;

  @SuppressWarnings("unused")
  public static void main(String[] args) throws IOException {
    init();
    int ops = crunchifySocket.validOps();
    SelectionKey selectKy = crunchifySocket.register(selector, ops, null);
    log.info("Server ready to use!");
    while (true) {
      selector.select();
      Set<SelectionKey> crunchifyKeys = selector.selectedKeys();
      Iterator<SelectionKey> crunchifyIterator = crunchifyKeys.iterator();

      while (crunchifyIterator.hasNext()) {
        SelectionKey myKey = crunchifyIterator.next();
        if (myKey.isAcceptable()) {
          handleAccept();
        } else if (myKey.isReadable()) {
          handleRead(myKey);
        }
      }
      crunchifyIterator.remove();
    }
  }

  private static void init() throws IOException {
    message = new Message("test");
    selector = Selector.open();
    users = new ArrayList<>();
    crunchifySocket = ServerSocketChannel.open();
    crunchifyAddr = new InetSocketAddress(HOST, PORT);
    crunchifySocket.bind(crunchifyAddr);
    crunchifySocket.configureBlocking(false);
  }

  private static void processWritingRequest(SelectionKey key, String messageToSend)
      throws IOException {
    SocketChannel cli = (SocketChannel) key.channel();
    System.out.println(String.format("Wrinting into the channel %s", cli));
    cli.write(ByteBuffer.wrap(new String(messageToSend).getBytes()));
  }

  private static void handleRead(SelectionKey key) throws IOException {
    SocketChannel crunchifyClient = (SocketChannel) key.channel();
    ByteBuffer crunchifyBuffer = ByteBuffer.allocate(256);
    crunchifyClient.read(crunchifyBuffer);
    String msg = new String(crunchifyBuffer.array()).trim();
    if (msg.equalsIgnoreCase("END")) {
      msg = ((SocketChannel) key.channel()).getRemoteAddress() + " left the chat";
      log.warn(msg);
      broadcast(msg);
      crunchifyClient.close();
      users.remove(crunchifyClient);
    } else {
      msg = "\n User" + crunchifyClient.getRemoteAddress() + " : " + msg;
      System.out.println(msg);
      broadcast(msg);
    }
  }

  private static void handleAccept() throws IOException {
    SocketChannel crunchifyClient = crunchifySocket.accept();
    users.add(crunchifyClient);
    crunchifyClient.configureBlocking(false);
    crunchifyClient.register(selector, SelectionKey.OP_READ);
    log.info("New user connected : " + crunchifyClient.getRemoteAddress() + ". Welcome!\n");
  }

  private static void broadcast(String msg) throws IOException {
    ByteBuffer msgBuf = ByteBuffer.wrap(msg.trim().getBytes());
    for (SocketChannel channel : users) {
      channel.write(msgBuf);
    }
  }

  public static Process start() throws IOException, InterruptedException {
    String javaHome = System.getProperty("java.home");
    String javaBin = javaHome + File.separator + "bin" + File.separator + "java";
    String classpath = System.getProperty("java.class.path");
    String className = NioServerController.class.getCanonicalName();

    ProcessBuilder builder = new ProcessBuilder(javaBin, "-cp", classpath, className);

    return builder.start();
  }
}
