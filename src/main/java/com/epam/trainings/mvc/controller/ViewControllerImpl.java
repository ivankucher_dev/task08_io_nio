package com.epam.trainings.mvc.controller;

import com.epam.trainings.mvc.model.Model;
import com.epam.trainings.mvc.model.submodels.Ship;
import com.epam.trainings.mvc.view.View;
import com.epam.trainings.utils.Serialize;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import static com.epam.trainings.utils.PropertiesGetter.*;
import static com.epam.trainings.utils.FileExtension.*;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class ViewControllerImpl implements ViewController {
  private static Logger log = LogManager.getLogger(ViewControllerImpl.class.getName());
  private View view;
  private Model model;
  private Map<Integer, Runnable> commands;
  private Map<Integer, String> commandsString;
  private Serialize<Ship> serializer;

  public ViewControllerImpl(View view, Model model) {
    this.view = view;
    this.model = model;
    commands = new HashMap<>();
    commandsString = new HashMap<>();
    serializer = new Serialize<>();
    initMethods();
  }

  public void execute(int command) {
    if (command < 0 || commands.size() + 1 < command) {
      view.showMessage("No Commands found");
      view.updateView();
      return;
    }
    commands.get(command).run();
    view.updateView();
  }

  @Override
  public String getAvaliableCommands() {
    StringBuilder sb = new StringBuilder();
    commandsString.forEach((k, v) -> sb.append("\n" + k + ". " + v));
    return sb.toString();
  }

  private void initMethods() {
    commands.put(1, this::serializableCommand);
    commandsString.put(1, "serializable task");
    commands.put(2, this::testBufferRW);
    commandsString.put(2, "Buffer test");
    commands.put(3, this::showDirectoryTree);
    commandsString.put(3, "Directory tree");
    commands.put(4, this::readCommentsFromJavaFile);
    commandsString.put(4, "Read comments from java file");
    commands.put(777, this::exit);
    commandsString.put(777, "exit");
  }

  private void testBufferRW() {
    Map map = model.runTextBuffer();
    StringBuilder sb = new StringBuilder();
    map.forEach((k, v) -> sb.append(v + " \n " + k + "\n"));
    view.showMessage(sb.toString());
  }

  public void showDirectoryTree() {
    StringBuilder sb = new StringBuilder();
    File directory = scanPathFromConsole();
    directoryTree(directory, 0, sb);
    view.showMessage(sb.toString());
  }

  private void directoryTree(File folder, int indent, StringBuilder sb) {
    if (!folder.isDirectory()) {
      log.warn("folder isnt  a directory");
      return;
    }
    sb.append("\n  ");
    sb.append("+--");
    sb.append(folder.getName());
    sb.append("/");
    sb.append("\n");
    for (File file : folder.listFiles()) {
      if (file.isDirectory()) {
        directoryTree(file, indent + 1, sb);
      } else {
        sb.append("|  ");
        sb.append("+--");
        sb.append(file.getName());
        sb.append("\n");
      }
    }
  }

  public void readCommentsFromJavaFile() {
    String text = " ";
    try {
      text = readFromJavaFile();
    } catch (IOException e) {
      e.printStackTrace();
    }
    view.showMessage(text);
  }

  private String readFromJavaFile() throws IOException {
    File file;
    do {
      file = scanPathFromConsole();

    } while (getExtensionByStringHandling(file.getName()).equals(".java"));

    StringBuilder text = new StringBuilder();
    try (FileInputStream fs = new FileInputStream(file)) {
      BufferedInputStream br = new BufferedInputStream(fs);
      BufferedReader r = new BufferedReader(new InputStreamReader(br, StandardCharsets.UTF_8));
      String s;
      while ((s = r.readLine()) != null) {
        if (s.contains("//")) text.append("\n" + s.substring(s.indexOf("//")));
      }
      r.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
    return text.toString();
  }

  private File scanPathFromConsole() {
    String dir;
    File directory;
    do {
      Scanner scanner = new Scanner(System.in);
      view.showMessage("Enter exist path directory to show tree(example : E:\\Java\\) : ");
      dir = scanner.nextLine();
      directory = new File(dir);
    } while (!directory.exists());
    return directory;
  }

  private void serializableCommand() {
    serializer.write(getProperty("file_ship"), model.getShip());
    model.getShip().setName("new name");
    Ship ship = serializer.read(getProperty("file_ship"));
    view.showMessage("Got from file : " + ship.toString());
  }

  private void exit() {
    System.exit(1);
  }
}
