package com.epam.trainings.mvc.controller;

public interface ViewController {
  void execute(int command);

  String getAvaliableCommands();
}
