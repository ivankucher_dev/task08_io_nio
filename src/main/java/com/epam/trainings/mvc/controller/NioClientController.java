package com.epam.trainings.mvc.controller;

import com.epam.trainings.mvc.model.submodels.ClientServerMessage;
import com.epam.trainings.mvc.model.submodels.Message;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.io.IOException;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Scanner;

public class NioClientController {
  private static Logger log = LogManager.getLogger(NioClientController.class.getName());
  private static ClientServerMessage messageToSend;
  private static ClientServerMessage messageToReceive;
  private static SocketChannel server;
  private static Selector sel;
  private static InetSocketAddress crunchifyAddr;
  private static Scanner sc;
  private static boolean done = false;

  public static void main(String[] args) throws IOException, InterruptedException {
    init();
    ByteBuffer bufferRead = ByteBuffer.allocate(256);
    while (!done) {
      write();
      sel.select();
      Iterator it = sel.selectedKeys().iterator();
      while (it.hasNext()) {
        SelectionKey key = (SelectionKey) it.next();
        server = (SocketChannel) key.channel();
        if (key.isConnectable() && !server.isConnected()) {
          InetAddress addr = InetAddress.getByName(null);
          boolean success = server.connect(new InetSocketAddress(addr, NioServerController.PORT));
          if (!success) server.finishConnect();
        }
        if (key.isReadable()) {
          server.read(bufferRead);
          messageToReceive.setMessage(new String(bufferRead.array()).trim());
          log.info(messageToReceive);
          bufferRead = ByteBuffer.allocate(256);
          if (isEnd()) done = true;
        }
      }
      it.remove();
    }

    stop();
  }

  private static boolean isEnd() {
    if (messageToSend.toString().equalsIgnoreCase("END")) {
      return true;
    } else {
      return false;
    }
  }

  private static void write() throws IOException, InterruptedException {
    messageToSend.setMessage(inputUserMessage());
    byte[] message = messageToSend.getMessageAsByteArray();
    ByteBuffer bufferWrite = ByteBuffer.wrap(message);
    server.write(bufferWrite);
    bufferWrite.clear();
    Thread.sleep(2000);
    if (isEnd()) {
      done = true;
    }
  }

  private static void init() throws IOException {
    sc = new Scanner(System.in);
    sel = Selector.open();
    messageToSend = new Message("test");
    messageToReceive = new Message("test receive");
    crunchifyAddr = new InetSocketAddress("localhost", 1111);
    server = SocketChannel.open(crunchifyAddr);
    server.configureBlocking(false);
    server.register(sel, SelectionKey.OP_READ | SelectionKey.OP_WRITE | SelectionKey.OP_CONNECT);
    log.info("Connecting to Server on port " + NioServerController.PORT);
  }

  private static String inputUserMessage() {
    log.info("Send massage : ");
    return sc.nextLine();
  }

  public static void stop() throws IOException {
    server.close();
    sel.close();
  }
}
