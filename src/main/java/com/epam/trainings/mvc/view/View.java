package com.epam.trainings.mvc.view;

import com.epam.trainings.mvc.controller.ViewController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Scanner;

public class View {
  private static Logger log = LogManager.getLogger(View.class.getName());
  private ViewController controller;
  private Scanner sc;

  public View() {
    this.sc = new Scanner(System.in);
  }

  public void setController(ViewController controller) {
    this.controller = controller;
    show();
  }

  public void updateView() {
    show();
  }

  public void showMessage(String message) {
    log.info(message);
  }

  public void show() {
    log.info(controller.getAvaliableCommands());
    log.info("Enter command : ");
    int choose = Integer.valueOf(sc.nextLine());
    controller.execute(choose);
  }
}
