package com.epam.trainings.main;

import com.epam.trainings.mvc.controller.ViewController;
import com.epam.trainings.mvc.controller.ViewControllerImpl;
import com.epam.trainings.mvc.model.Model;
import com.epam.trainings.mvc.view.View;

public class Main {
    public static void main(String[] args) {
        View view = new View();
        Model model = new Model();
        ViewController controller = new ViewControllerImpl(view,model);
        view.setController(controller);

    }

}
